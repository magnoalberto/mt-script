/*
Copyright 2019 FXcoder

This file is part of Index.

Index is free software: you can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Index is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with Index. If not, see
http://www.gnu.org/licenses/.
*/

// Класс доступа к свойстам терминала, 5.2005/4.1601. Better Standard Library. © FXcoder


#property strict

#include "chart.mqh"
#include "mql.mqh"
#include "window.mqh"


class CBTerminal
{
public:

	// функции терминала
	static bool close(int ret_code) { return(::TerminalClose(ret_code)); }

	// свойства int/bool
	static int  build                 () { return(        ::TerminalInfoInteger(TERMINAL_BUILD                 )); }
	static bool community_account     () { return( (bool) ::TerminalInfoInteger(TERMINAL_COMMUNITY_ACCOUNT     )); }
	static bool community_connection  () { return( (bool) ::TerminalInfoInteger(TERMINAL_COMMUNITY_CONNECTION  )); }
	static int  codepage              () { return(        ::TerminalInfoInteger(TERMINAL_CODEPAGE              )); }
	static bool connected             () { return( (bool) ::TerminalInfoInteger(TERMINAL_CONNECTED             )); }
	static int  cpu_cores             () { return(        ::TerminalInfoInteger(TERMINAL_CPU_CORES             )); }
	static int  disk_space            () { return(        ::TerminalInfoInteger(TERMINAL_DISK_SPACE            )); }
	static bool dlls_allowed          () { return( (bool) ::TerminalInfoInteger(TERMINAL_DLLS_ALLOWED          )); }
	static bool email_enabled         () { return( (bool) ::TerminalInfoInteger(TERMINAL_EMAIL_ENABLED         )); }
	static bool ftp_enabled           () { return( (bool) ::TerminalInfoInteger(TERMINAL_FTP_ENABLED           )); }
	static int  max_bars              () { return(        ::TerminalInfoInteger(TERMINAL_MAXBARS               )); }
	static int  memory_available      () { return(        ::TerminalInfoInteger(TERMINAL_MEMORY_AVAILABLE      )); }
	static int  memory_physical       () { return(        ::TerminalInfoInteger(TERMINAL_MEMORY_PHYSICAL       )); }
	static int  memory_total          () { return(        ::TerminalInfoInteger(TERMINAL_MEMORY_TOTAL          )); }
	static int  memory_used           () { return(        ::TerminalInfoInteger(TERMINAL_MEMORY_USED           )); }
	static bool mqid                  () { return( (bool) ::TerminalInfoInteger(TERMINAL_MQID                  )); }
	static bool notifications_enabled () { return( (bool) ::TerminalInfoInteger(TERMINAL_NOTIFICATIONS_ENABLED )); }
	static int  onencl_support        () { return(        ::TerminalInfoInteger(TERMINAL_OPENCL_SUPPORT        )); }
	static int  ping_last             () { return(        ::TerminalInfoInteger(TERMINAL_PING_LAST             )); }
	static int  screen_dpi            () { return(        ::TerminalInfoInteger(TERMINAL_SCREEN_DPI            )); }
	static bool trade_allowed         () { return( (bool) ::TerminalInfoInteger(TERMINAL_TRADE_ALLOWED         )); }
	static bool x64                   () { return( (bool) ::TerminalInfoInteger(TERMINAL_X64                   )); }

	// клавиши
	static int key_state_left       () { return(::TerminalInfoInteger(TERMINAL_KEYSTATE_LEFT     )); }
	static int key_state_up         () { return(::TerminalInfoInteger(TERMINAL_KEYSTATE_UP       )); }
	static int key_state_right      () { return(::TerminalInfoInteger(TERMINAL_KEYSTATE_RIGHT    )); }
	static int key_state_down       () { return(::TerminalInfoInteger(TERMINAL_KEYSTATE_DOWN     )); }
	static int key_state_shift      () { return(::TerminalInfoInteger(TERMINAL_KEYSTATE_SHIFT    )); }
	static int key_state_control    () { return(::TerminalInfoInteger(TERMINAL_KEYSTATE_CONTROL  )); }
	static int key_state_menu       () { return(::TerminalInfoInteger(TERMINAL_KEYSTATE_MENU     )); }
	static int key_state_capslock   () { return(::TerminalInfoInteger(TERMINAL_KEYSTATE_CAPSLOCK )); }
	static int key_state_numlock    () { return(::TerminalInfoInteger(TERMINAL_KEYSTATE_NUMLOCK  )); }
	static int key_state_scrlock    () { return(::TerminalInfoInteger(TERMINAL_KEYSTATE_SCRLOCK  )); }
	static int key_state_enter      () { return(::TerminalInfoInteger(TERMINAL_KEYSTATE_ENTER    )); }
	static int key_state_insert     () { return(::TerminalInfoInteger(TERMINAL_KEYSTATE_INSERT   )); }
	static int key_state_delete     () { return(::TerminalInfoInteger(TERMINAL_KEYSTATE_DELETE   )); }
	static int key_state_home       () { return(::TerminalInfoInteger(TERMINAL_KEYSTATE_HOME     )); }
	static int key_state_end        () { return(::TerminalInfoInteger(TERMINAL_KEYSTATE_END      )); }
	static int key_state_tab        () { return(::TerminalInfoInteger(TERMINAL_KEYSTATE_TAB      )); }
	static int key_state_pageup     () { return(::TerminalInfoInteger(TERMINAL_KEYSTATE_PAGEUP   )); }
	static int key_state_pagedown   () { return(::TerminalInfoInteger(TERMINAL_KEYSTATE_PAGEDOWN )); }
	static int key_state_escape     () { return(::TerminalInfoInteger(TERMINAL_KEYSTATE_ESCAPE   )); }

	// свойства double
	static double community_balance () { return(::TerminalInfoDouble(TERMINAL_COMMUNITY_BALANCE)); }

	// свойства string
	static string language          () { return(::TerminalInfoString(TERMINAL_LANGUAGE        )); }
	static string company           () { return(::TerminalInfoString(TERMINAL_COMPANY         )); }
	static string name              () { return(::TerminalInfoString(TERMINAL_NAME            )); }
	static string path              () { return(::TerminalInfoString(TERMINAL_PATH            )); }
	static string data_path         () { return(::TerminalInfoString(TERMINAL_DATA_PATH       )); }
	static string common_data_path  () { return(::TerminalInfoString(TERMINAL_COMMONDATA_PATH )); }

	// Дополнительные функции и свойства

	static int window_handle()
	{
#ifdef DEBUG
		return(0);
#else
		if (!_mql.dlls_allowed())
			return(0);
		
		CBWindow w(_chart.window_handle());
		return(w.root_parent_hadle());
#endif
	}

	static bool is_maximized(bool fallback)
	{
#ifdef DEBUG
		return(fallback);
#else
		if (!_mql.dlls_allowed())
			return(fallback);

		CBWindow w(CBTerminal::window_handle());
		if (!w.is_valid())
			return(fallback);
		
		return(w.is_maximized());
#endif
	}

	static bool is_minimized(bool fallback)
	{
#ifdef DEBUG
		return(fallback);
#else
		if (!_mql.dlls_allowed())
			return(fallback);

		CBWindow w(CBTerminal::window_handle());
		if (!w.is_valid())
			return(fallback);
		
		return(w.is_minimized());
#endif
	}

	// клавиши
	static bool is_left_key_pressed       () { return(is_key_pressed(TERMINAL_KEYSTATE_LEFT     )); }
	static bool is_up_key_pressed         () { return(is_key_pressed(TERMINAL_KEYSTATE_UP       )); }
	static bool is_right_key_pressed      () { return(is_key_pressed(TERMINAL_KEYSTATE_RIGHT    )); }
	static bool is_down_key_pressed       () { return(is_key_pressed(TERMINAL_KEYSTATE_DOWN     )); }
	static bool is_shift_key_pressed      () { return(is_key_pressed(TERMINAL_KEYSTATE_SHIFT    )); }
	static bool is_control_key_pressed    () { return(is_key_pressed(TERMINAL_KEYSTATE_CONTROL  )); }
	static bool is_menu_key_pressed       () { return(is_key_pressed(TERMINAL_KEYSTATE_MENU     )); }
	static bool is_capslock_key_pressed   () { return(is_key_pressed(TERMINAL_KEYSTATE_CAPSLOCK )); }
	static bool is_numlock_key_pressed    () { return(is_key_pressed(TERMINAL_KEYSTATE_NUMLOCK  )); }
	static bool is_scrlock_key_pressed    () { return(is_key_pressed(TERMINAL_KEYSTATE_SCRLOCK  )); }
	static bool is_enter_key_pressed      () { return(is_key_pressed(TERMINAL_KEYSTATE_ENTER    )); }
	static bool is_insert_key_pressed     () { return(is_key_pressed(TERMINAL_KEYSTATE_INSERT   )); }
	static bool is_delete_key_pressed     () { return(is_key_pressed(TERMINAL_KEYSTATE_DELETE   )); }
	static bool is_home_key_pressed       () { return(is_key_pressed(TERMINAL_KEYSTATE_HOME     )); }
	static bool is_end_key_pressed        () { return(is_key_pressed(TERMINAL_KEYSTATE_END      )); }
	static bool is_tab_key_pressed        () { return(is_key_pressed(TERMINAL_KEYSTATE_TAB      )); }
	static bool is_pageup_key_pressed     () { return(is_key_pressed(TERMINAL_KEYSTATE_PAGEUP   )); }
	static bool is_pagedown_key_pressed   () { return(is_key_pressed(TERMINAL_KEYSTATE_PAGEDOWN )); }
	static bool is_escape_key_pressed     () { return(is_key_pressed(TERMINAL_KEYSTATE_ESCAPE   )); }


protected:

	// Проверить, нажата ли клавиша (не путать с включенной, например клавиши *Lock, они проверяются по младшему биту)
	static bool is_key_pressed(ENUM_TERMINAL_INFO_INTEGER key) { return((::TerminalInfoInteger(key) & 0x80) != 0); }
		
} _terminal;


#ifndef __MQL4__

class CBTerminal5: public CBTerminal
{
public:

	// Свойства

	// 5.1730+
	static double retransmission  () { return(::TerminalInfoDouble(TERMINAL_RETRANSMISSION)); }

	// 5.1910
	static int screen_left   () { return(::TerminalInfoInteger(TERMINAL_SCREEN_LEFT  )); }
	static int screen_top    () { return(::TerminalInfoInteger(TERMINAL_SCREEN_TOP   )); }
	static int screen_width  () { return(::TerminalInfoInteger(TERMINAL_SCREEN_WIDTH )); }
	static int screen_height () { return(::TerminalInfoInteger(TERMINAL_SCREEN_HEIGHT)); }

	static int left   () { return(::TerminalInfoInteger(TERMINAL_SCREEN_LEFT  )); }
	static int top    () { return(::TerminalInfoInteger(TERMINAL_SCREEN_TOP   )); }
	static int width  () { return(::TerminalInfoInteger(TERMINAL_SCREEN_WIDTH )); }
	static int height () { return(::TerminalInfoInteger(TERMINAL_SCREEN_HEIGHT)); }

	// 5.2005
	static bool vps   () { return((bool)::TerminalInfoInteger(TERMINAL_VPS)); }

} _terminal5;

#endif

