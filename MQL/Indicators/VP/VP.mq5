/*
Copyright 2019 FXcoder

This file is part of VP.

VP is free software: you can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

VP is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with VP. If not, see
http://www.gnu.org/licenses/.
*/

#property copyright "VP: Volume Profile v7.0.1. © FXcoder"
#property link      "https://fxcoder.blogspot.com"
#property strict
#property indicator_chart_window
#property indicator_plots 0

//#define DEBUG

#include "VP-include/bsl.mqh"
#include "VP-include/class/timer.mqh"
#include "VP-include/enum/hg_direction.mqh"
#include "VP-include/enum/point_scale.mqh"
#include "VP-include/util/stat.mqh"
#include "VP-include/volume/enum/vp_bar_style.mqh"
#include "VP-include/volume/enum/vp_hg_position.mqh"
#include "VP-include/volume/enum/vp_mode.mqh"
#include "VP-include/volume/enum/vp_range_mode.mqh"
#include "VP-include/volume/enum/vp_time_shift.mqh"
#include "VP-include/volume/enum/vp_zoom.mqh"
#include "VP-include/volume/src_mt.mqh"
#include "VP-include/volume/vp_util.mqh"


/* INPUT */

//#define input

#ifndef INPUT_GROUP
#define INPUT_GROUP ""
#endif

input ENUM_VP_MODE           Mode           = VP_MODE_PERIOD;        // Mode

input string                 g_per_mode_    = INPUT_GROUP;           // •••••••••• PERIOD MODE ••••••••••
input ENUM_TIMEFRAMES        RangePeriod    = PERIOD_D1;             // Range Period
input int                    RangeCount     = 20;                    // Range Count
input ENUM_VP_TIME_SHIFT     TimeShift      = VP_TIME_SHIFT_0;       // Time Shift
input ENUM_HG_DIRECTION      DrawDirection  = HG_DIRECTION_RIGHT;    // Draw Direction
input ENUM_VP_ZOOM           ZoomType       = VP_ZOOM_AUTO_SEPARATE; // Zoom Type
input double                 ZoomCustom     = 0;                     // Custom Zoom

input string                 g_rng_mode_    = INPUT_GROUP;                     // •••••••••• RANGE MODE ••••••••••
input ENUM_VP_RANGE_MODE     RangeMode      = VP_RANGE_MODE_BETWEEN_LINES;     // Range Mode
input int                    RangeMinutes   = 1440;                            // Range Minutes
input ENUM_VP_HG_POSITION    HgPosition     = VP_HG_POSITION_WINDOW_RIGHT;     // Histogram Position
input double                 HgWidthPercent = 15;                              // Histogram Width (% of chart)

input string                 g_calc_        = INPUT_GROUP;           // •••••••••• CALCULATION ••••••••••
input int                    ModeStep       = 100;                   // Mode Step (points)
input ENUM_POINT_SCALE       HgPointScale   = POINT_SCALE_10;        // Point Scale

#ifdef __MQL4__
      ENUM_APPLIED_VOLUME    VolumeType     = VOLUME_TICK;           // Volume Type (always TICK in 4)
#else
input ENUM_APPLIED_VOLUME    VolumeType     = VOLUME_TICK;           // Volume Type
#endif

input ENUM_VP_SOURCE         DataSource     = VP_SOURCE_M1;          // Data Source
input int                    Smooth         = 0;                     // Smooth Depth (0 => disable)

input string                 g_hg_          = INPUT_GROUP;           // •••••••••• HISTOGRAM ••••••••••
input ENUM_VP_BAR_STYLE      HgBarStyle     = VP_BAR_STYLE_LINE;     // Bar Style
input color                  HgColor        = C'128,160,192';        // Color 1
input color                  HgColor2       = C'128,160,192';        // Color 2
input int                    HgLineWidth    = 1;                     // Line Width

input string                 g_levels_      = INPUT_GROUP; // •••••••••• LEVELS ••••••••••
input color                  ModeColor      = clrBlue;     // Mode Color
input color                  MaxColor       = clrNONE;     // Maximum Color
input color                  MedianColor    = clrNONE;     // Median Color
input color                  VwapColor      = clrNONE;     // VWAP Color
input int                    ModeLineWidth  = 1;           // Mode Line Width
input ENUM_LINE_STYLE        StatLineStyle  = STYLE_DOT;   // Median & VWAP Line Style

input string                 g_lev_lines_   = INPUT_GROUP; // •••••••••• LEVEL LINES (range mode only) ••••••••••
input color                  ModeLevelColor = clrGreen;    // Mode Level Line Color (None=disable)
input int                    ModeLevelWidth = 1;           // Mode Level Line Width
input ENUM_LINE_STYLE        ModeLevelStyle = STYLE_SOLID; // Mode Level Line Style

input string                 g_service_     = INPUT_GROUP; // •••••••••• SERVICE ••••••••••
input bool                   ShowHorizon    = true;        // Show Data Horizon
input string                 Id             = "+vp";       // Identifier


/* GLOBALS */

// common
bool   is_timeframe_enabled_ = false;
bool   is_range_mode_        = Mode == VP_MODE_RANGE;
ENUM_TIMEFRAMES data_period_ = PERIOD_M1;
string hz_line_name_         = Id + "-" + string((int(DataSource))) + "-hz";
string prefix_               = Id + (is_range_mode_ ? " m" + IntegerToString(RangeMode) : " " + IntegerToString(RangePeriod)) + " ";
bool   last_ok_              = false;
int    first_visible_bar_    = 0;
int    last_visible_bar_     = 0;
int    mode_step_            = ModeStep / HgPointScale;

CTimer update_timer_(is_range_mode_ ? 500 : 1000, false);
CVPUtil vp_
(
	Id, HgPointScale, HgBarStyle, HgColor, HgColor2, HgLineWidth,
	ModeColor, MaxColor, MedianColor, VwapColor, ModeLineWidth, StatLineStyle,
	is_range_mode_ ? ModeLevelColor : clrNONE, ModeLevelWidth, ModeLevelStyle
);

// period mode
int      time_shift_seconds_ = 0;
int      range_count_        = RangeCount > 0 ? RangeCount : 1;
datetime draw_history_[]; // история рисования
double   overall_max_volume_ = 1; // overall max volume

// range mode
bool update_on_tick_ = true;

class CVPHistogram
{
public:

	double low_price;
	double volumes[];
	double max_volume;
	int bar_from;
	int bar_to;
	string prefix;
	bool need_redraw;
	
	void CVPHistogram():
		low_price(0), max_volume(1), bar_from(0), bar_to(0), prefix(""), need_redraw(false)
	{
	}

} *hgs_[];


/* EVENTS */

void OnInit()
{
	// таймфрейм источника данных
	data_period_ = get_data_period(DataSource);

	/* Period mode */

	// сдвиг не может быть больше RangePeriod
	time_shift_seconds_ = (((int)TimeShift * 60) % PeriodSeconds(RangePeriod));

	// коррекция сдвига вперёд, если он отрицательный, чтобы рисовался и последний диапазон
	if (time_shift_seconds_ < 0)
		time_shift_seconds_ += PeriodSeconds(RangePeriod);
		
	// delete range line after switching to period mode
	if (!is_range_mode_)
		vp_.delete_range_lines();
}

void OnChartEvent(const int id, const long &lparam, const double &dparam, const string &sparam)
{
#ifdef __MQL4__
	if (!is_timeframe_enabled_)
		return;
#endif

	CBChartEvent event(id, lparam, dparam, sparam);

	if (is_range_mode_)
	{
		// Если сдвинули границы выделение, обновить индикатор
		if (event.is_object_move_event(vp_.line_from().name()) || event.is_object_move_event(vp_.line_to().name()))
		{
	 		check_timer();
		}
		
		// изменение графика (масштаб, положение, цвет фона).
		if (event.is_chart_change_event())
		{
			int first_visible_bar = _chart.first_visible_bar();
			int last_visible_bar = _chart.last_visible_bar();
			
			bool update =
				(first_visible_bar_ == last_visible_bar_) ||
				(
					((first_visible_bar != first_visible_bar_) || (last_visible_bar != last_visible_bar_)) &&
					((HgPosition == VP_HG_POSITION_WINDOW_LEFT) || (HgPosition == VP_HG_POSITION_WINDOW_RIGHT))
				);
			
			first_visible_bar_ = first_visible_bar;
			last_visible_bar_ = last_visible_bar;
			
			if (vp_.update_auto_colors())
			{
				last_ok_ = false;
				check_timer();
			}
			else if (update)
			{
				check_timer();
			}
		}
	}
	else
	{
		if (event.is_chart_change_event())
		{
			// если цвет фона изменился, перерисовать все гистограммы
			if (vp_.update_auto_colors())
			{
				ArrayFree(draw_history_);
				check_timer();
			}
		}
	}
}

int OnCalculate(const int rates_total, const int prev_calculated, const datetime &time[], const double &open[], const double &high[], const double &low[], const double &close[], const long &tick_volume[], const long &volume[], const int &spread[])
{
#ifdef __MQL4__
	// Хак против бага в MT4: при отключении отображения на ТФ отключается только OnCalculate
	is_timeframe_enabled_ = true;
#endif

	if (is_range_mode_)
	{
		if (vp_.update_auto_colors())
		{
			// при обновлении цвета фона обновлять сразу, сбросив признак последнего успешного выполнения
			last_ok_ = false;
			check_timer();
		}
		else if (update_on_tick_)
		{
			// обновлять на каждом тике, если выставлен такой признак при анализе правой границы
			check_timer();
		}
	}
	else
	{
		// если цвет фона изменился, перерисовать все гистограммы
		if (vp_.update_auto_colors())
			ArrayFree(draw_history_);

		check_timer();
	}

	return(0);
}

void OnTimer()
{
#ifdef __MQL4__
	if (!is_timeframe_enabled_)
		return;
#endif

	check_timer();
}

void OnDeinit(const int reason)
{
	// удалить гистограммы и их производные
	_chart.objects_delete_all(prefix_);
	_go[hz_line_name_].del();

	if (is_range_mode_)
	{
		// удалить линии только при явном удалении индикатора с графика
		if (reason == REASON_REMOVE)
			vp_.delete_range_lines();
	}
}


/* WORKERS */

/**
Проверить таймер. Если сработал, обновить индикатор.
*/
void check_timer()
{
	// Выключить резервный таймер
	_event.kill_timer();

	// Если таймер сработал, нарисовать картинку. Либо сразу, если в прошлый раз была проблема.
	if (update_timer_.check() || !last_ok_)
	{
		// Обновить. В случае неудачи поставить таймер на 3 секунды, чтобы попробовать снова ещё раз.
		// 3 секунды должно быть достаточно для подгрузки последней истории. Иначе всё просто повторится ещё через 3.
		last_ok_ = is_range_mode_ ? update_range() : update();
		
		if (!last_ok_)
			_event.set_timer(3);
		
		_chart.redraw();
		
		// расчёт и рисование могут быть длительными, лучше перезапустить таймер
		update_timer_.reset();
	}
	else
	{
		// На случай, если свой таймер больше не будет проверяться, добавить принудительную проверку через 1-3 секунды
		_event.set_timer(is_range_mode_ ? 1 : 3);
	}
}

/**
Update (period mode).
@return  false on fail (no rates).
*/
bool update()
{
	// Составить список диапазонов
	datetime ranges[];
	ArraySetAsSeries(ranges, true);
	ArrayResize(ranges, range_count_);

	if (CopyTime(_Symbol, RangePeriod, 0, range_count_, ranges) != range_count_)
		return(false);
	
	// Если RangePeriod - неделя, сдвинуть всё вперёд на 1 день, т.к. неделя в MT начинается с воскресенья
	if (RangePeriod == PERIOD_W1)
	{
		for (int i = 0; i < range_count_; i++)
			ranges[i] += _time.seconds_in_day;
	}

	/* Проверить наличие необходимых котировок. */
	int range_seconds = PeriodSeconds(RangePeriod);

	datetime data_rates[];
	datetime ranges_start = ranges[range_count_ - 1] + time_shift_seconds_;
	datetime ranges_end = ranges[0] + range_seconds - 1 + time_shift_seconds_;

	// reset and redraw everything on new histogram
	static datetime prev_range0 = 0;
	if (prev_range0 != ranges[0])
	{
LOG("New histogram");
		ArrayFree(draw_history_);
		prev_range0 = ranges[0];
		_chart.objects_delete_all(prefix_);
		overall_max_volume_ = 1;
	}

	if (CopyTime(_Symbol, data_period_, ranges_start, ranges_end, data_rates) <= 0)
	{
		LOG("!CopyTime: " + VAR(ranges_start) + VAR(ranges_end));
		return(false);
	}

	// время текущего бара
	MqlTick tick;
	if (!_symbol.tick(tick))
		return(false);

	datetime last_tick_time = tick.time;

	if (ShowHorizon)
	{
		datetime horizon = get_horizon(DataSource, data_period_);
		vp_.draw_horizon(hz_line_name_, horizon);
	}

	int modes[];
	bool total_result = true;
	ArrayResize(hgs_, range_count_);
	
	for (int i = 0; i < range_count_; i++)
	{
		CVPHistogram *hg = new CVPHistogram();
		hgs_[i] = hg;
	
		/* Расчёт */
		
		// Границы диапазона
		datetime range_start = ranges[i] + time_shift_seconds_;
		
		if (_time.day_of_week(range_start) == SUNDAY)
			range_start = _time.add_days(range_start, -2);
		
		// Если гистограмма уже была успешно нарисована, пропустить. Кроме крайней правой.
		if ((i != 0) && (_arr.contains(draw_history_, range_start)))
			continue;
		
		
		// конец диапазона с учётом сдвига
		datetime range_end;

		if (i == 0)
		{
			range_end = ranges[i] + time_shift_seconds_ + range_seconds - 1;
		}
		else
		{
			range_end = ranges[i - 1] + time_shift_seconds_ - 1;
			
			if (_time.day_of_week(range_end) == SUNDAY)
				range_end = _time.add_days(range_end, -2);
		}


		if (!vp_.get_range_bars(range_start, range_end, hg.bar_from, hg.bar_to))
		{
			total_result = false;
			// в случае ошибки рисования одной гистограммы, продолжить рисовать другие
			LOG("!vp_.get_range_bars: " + VAR(range_start) + VAR(range_end) + VAR(hg.bar_from) + VAR(hg.bar_to));
			continue;
		}


#ifndef __MQL4__
		int count;

		if (DataSource == VP_SOURCE_TICKS)
			count = get_hg_by_ticks(range_start, range_end, vp_.hg_point(), VolumeType, hg.low_price, hg.volumes);
		else
			count = get_hg(range_start, range_end, vp_.hg_point(), data_period_, VolumeType, hg.low_price, hg.volumes);
#else
		int count = get_hg(range_start, range_end, vp_.hg_point(), data_period_, VolumeType, hg.low_price, hg.volumes);
#endif
		
		if (count <= 0)
		{
			total_result = false;
			// в случае ошибки рисования одной гистограммы, продолжить рисовать другие
			LOG("!get_hg/get_hg_by_ticks: " + VAR(range_start) + VAR(range_end));
			continue;
		}
		
		if (Smooth > 0)
			count = vp_.smooth_hg(Smooth, hg.volumes);
			
		// добавить гистограмму в список выполненных, если её правая граница левее текущего тика
		if (range_end < last_tick_time)
			_arr.set(draw_history_, range_start);
			

		// префикс для каждой гистограммы свой
		hg.need_redraw = true;
		hg.prefix = prefix_ + (string)((int)range_start / PeriodSeconds(RangePeriod)) + " ";
		hg.max_volume = _arr.max_value(hg.volumes);
		
		// Учесть нулевые объёмы всех баров источника
		if (hg.max_volume <= 0)
			hg.max_volume = 1;
		
		if (hg.max_volume > overall_max_volume_)
			overall_max_volume_ = hg.max_volume;
	}

LOG(VAR(overall_max_volume_));

	// Рисование отдельно, т.к. может быть нужно значение максимального максимума (overall_max_volume_)
	for (int i = 0; i < range_count_; i++)
	{
		/* Отображение */
		CVPHistogram *hg = hgs_[i];

		if (!hg.need_redraw)
			continue;

		// Уровни
		int mode_count  = vp_.show_modes()  ? hg_modes(hg.volumes, mode_step_, modes)           : -1;
		int max_pos     = vp_.show_max()    ? _arr.max(hg.volumes)                              : -1;
		int median_pos  = vp_.show_median() ? _math.median_index(hg.volumes)                    : -1;
		int vwap_pos    = vp_.show_vwap()   ? hg_vwap(hg.volumes, hg.low_price, vp_.hg_point()) : -1;
		
		// определить масштаб и направление
		double zoom = ZoomCustom;
		
		if (ZoomType == VP_ZOOM_AUTO_OVERALL)
		{
			zoom = (hg.bar_from - hg.bar_to) / overall_max_volume_;
		}
		else if (ZoomType == VP_ZOOM_AUTO_SEPARATE)
		{
			zoom = (hg.bar_from - hg.bar_to) / hg.max_volume;
		}
		
		if (DrawDirection == HG_DIRECTION_LEFT)
			_bsl.swap(hg.bar_from, hg.bar_to);

		// Удалить старые линии
		_chart.objects_delete_all(hg.prefix);

		// Отобразить гистограмму
		vp_.draw_hg(hg.prefix, hg.low_price, hg.volumes, hg.bar_from, hg.bar_to, zoom, modes, max_pos, median_pos, vwap_pos);
	}
	
	_ptr.safe_delete_array(hgs_);

	return(total_result);
}

/**
Update (range mode).
@return  false on fail (no rates, wrong params).
*/
bool update_range()
{
	// удаляем старые объекты
	_chart.objects_delete_all(prefix_);

	// Определить рабочий диапазон

	datetime time_from, time_to;

	if (RangeMode == VP_RANGE_MODE_BETWEEN_LINES)  // между двух линий
	{
		// найти линии границ
		time_from = vp_.line_from().time1();
		time_to = vp_.line_to().time1();

		if ((time_from == 0) || (time_to == 0))
		{
			// если границы диапазона не заданы, установить их заново в видимую часть экрана
			datetime time_left  = _series.bar_time(_chart.first_visible_bar());
			datetime time_right = _series.bar_time(_chart.last_visible_bar());
			ulong time_range = time_right - time_left;
			
			time_from = (datetime)(time_left + time_range / 3);
			time_to = (datetime)(time_left + time_range * 2 / 3);

			// нарисовать линии
			vp_.draw_range_lines(time_from, time_to);
		}
		
		vp_.enable_range_lines();

		// если линии перепутаны местами, поменять местами времена начала и конца
		if (time_from > time_to)
			_bsl.swap(time_from, time_to);
	}
	else if (RangeMode == VP_RANGE_MODE_MINUTES_TO_LINE)  // от правой линии RangeMinutes минут
	{
		// найти правую линию
		time_to = vp_.line_to().time1();
		int bar;
		
		if (time_to == 0)
		{
			// если линии нет, установить его в видимую часть экрана
			int left_bar = _chart.first_visible_bar();
			int right_bar = _chart.last_visible_bar();
			int bar_range = left_bar - right_bar;
			
			bar = fmax(0, left_bar - bar_range / 3);
			time_to = _series.bar_time(bar);
		}
		else
		{
			bar = _series.bar_shift(time_to);
		}

		bar += RangeMinutes / _tf.minutes;
		time_from = _series.bar_time(bar);

		vp_.draw_line_from(time_from);
		
		// нарисовать левую границу и отключить возможность её выделения
		if (!vp_.line_to().exists())
		{
			vp_.draw_line_to(time_to);
		}

		vp_.disable_line_from();
		vp_.enable_line_to();
	}
	else if (RangeMode == VP_RANGE_MODE_LAST_MINUTES)
	{
		CBSeries m1_ser(_Symbol, PERIOD_M1);
		time_from = m1_ser.bar_time(RangeMinutes - 1);
		time_to = m1_ser.bar_time(-1);
		
		// удалить линии границ
		vp_.delete_range_lines();
	}
	else
	{
		return(false);
	}


	if (ShowHorizon)
	{
		datetime horizon = get_horizon(DataSource, data_period_);
		vp_.draw_horizon(hz_line_name_, horizon);
	}

	int bar_from, bar_to;

	if (!vp_.get_range_bars(time_from, time_to, bar_from, bar_to))
		return(false);

	// если правая граница правее нулевого бара, то гистограмму обновлять на каждом тике
	update_on_tick_ = bar_to < 0;

	// получаем гистограмму
	int modes[];
	double volumes[];
	double low_price;


#ifndef __MQL4__
	int count;

	if (DataSource == VP_SOURCE_TICKS)
		count = get_hg_by_ticks(time_from, time_to - 1, vp_.hg_point(), VolumeType, low_price, volumes);
	else
		count = get_hg(time_from, time_to - 1, vp_.hg_point(), data_period_, VolumeType, low_price, volumes);
#else
	int count = get_hg(time_from, time_to - 1, vp_.hg_point(), data_period_, VolumeType, low_price, volumes);
#endif

	if (count <= 0)
		return(false);

	if (Smooth != 0)
	{
		count = vp_.smooth_hg(Smooth, volumes);
		low_price -= Smooth * vp_.hg_point();
	}


	// Уровни
	int mode_count  = vp_.show_modes()  ? hg_modes(volumes, mode_step_, modes)        : -1;
	int max_pos     = vp_.show_max()    ? _arr.max(volumes)                          : -1;
	int median_pos  = vp_.show_median() ? _math.median_index(volumes)                : -1;
	int vwap_pos    = vp_.show_vwap()   ? hg_vwap(volumes, low_price, vp_.hg_point()) : -1;
		
				
	string prefix = prefix_ + (string)((int)RangeMode) + " ";
	double hg_width_bars = ((HgPosition == VP_HG_POSITION_LEFT_INSIDE) || (HgPosition == VP_HG_POSITION_RIGHT_INSIDE))
		? (bar_from - bar_to)
		: _chart.width_in_bars() * (HgWidthPercent / 100.0);

	double max_volume = _arr.max_value(volumes);

	// Учесть нулевые объёмамы всех баров источника
	if (max_volume == 0)
		max_volume = 1;

	// Определить масштаб. В Range Mode обе автоматики одинаковы, т.к. гг одна
	double zoom = (ZoomType == VP_ZOOM_CUSTOM) ? ZoomCustom : (hg_width_bars / max_volume);

	// Крайние бары оторбражения гистограммы
	int draw_bar_from, draw_bar_to;

	if (HgPosition == VP_HG_POSITION_WINDOW_LEFT)
	{
		// левая граница окна [> |  |  ]
		draw_bar_from = _chart.first_visible_bar();
		draw_bar_to = (int)(draw_bar_from - zoom * max_volume);
	}
	else if (HgPosition == VP_HG_POSITION_WINDOW_RIGHT)
	{
		// правая граница окна [  |  | <]
		draw_bar_from = _chart.last_visible_bar();
		draw_bar_to = (int)(draw_bar_from + zoom * max_volume);
	}
	else if (HgPosition == VP_HG_POSITION_LEFT_OUTSIDE)
	{
		// левая граница диапазона влево наружу [  <|  |  ]
		draw_bar_from = bar_from;
		draw_bar_to = (int)(draw_bar_from + zoom * max_volume);
	}
	else if (HgPosition == VP_HG_POSITION_RIGHT_OUTSIDE)
	{
		// правая граница диапазона наружу [   |  |>  ]
		draw_bar_from = bar_to;
		draw_bar_to = (int)(draw_bar_from - zoom * max_volume);
	}
	else if (HgPosition == VP_HG_POSITION_LEFT_INSIDE)
	{
		// левая граница диапазона влево внутрь [   |>  |  ]
		draw_bar_from = bar_from;
		draw_bar_to = bar_to;
	}
	else //if (HgPosition == VP_HG_POSITION_RIGHT_INSIDE)
	{
		// правая граница диапазона [   | <|  ]
		draw_bar_from = bar_to;
		draw_bar_to = bar_from;
	}

	// Отобразить гистограмму
	vp_.draw_hg(prefix, low_price, volumes, draw_bar_from, draw_bar_to, zoom, modes, max_pos, median_pos, vwap_pos);

	return(true);
}

/**
Получить таймфрейм источника данных
*/
ENUM_TIMEFRAMES get_data_period(ENUM_VP_SOURCE data_source)
{
#ifndef __MQL4__
	// без разницы
	if (data_source == VP_SOURCE_TICKS)
		return(PERIOD_CURRENT);
#endif

	return(_tf.find_closest((int)data_source));
}


/*
Последние изменения

7.0:
	* VP и VP-Range объединены в один индикатор VP, переключение параметром Mode
	* улучшены подсказки
	* ZoomType, ZoomCustom: тип масштабирования гистограмм: пользовательский (одинаковый для всех), авто (отдельно для каждой), авто (для всех в целом)
	* исправлено: не удаляются старые гистограммы (левее первой), может быть актуально для экономии ресурсов в визуальном тестере (Period Mode)
	* в 5 расширен набор источников данных (DataSource), добавлены промежуточные таймфреймы M2, M3 и подобные
	* ShowHorizon: показывать горизонт данных
	* ModeLevelWidth: толщина линий уровней мод (Range Mode)
	* HgWidthPercent (Range Mode): ширина гистограммы в процентах от ширины графика
	* Smooth: сглаживание
	* убран параметр VolumeType в 4, т.к. бесполезен

6.1:
	* совместимость с MT5 билд 1845 (где добавлены iTime, iBarShift)
*/
