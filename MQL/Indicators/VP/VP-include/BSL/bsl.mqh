/*
Copyright 2019 FXcoder

This file is part of VP.

VP is free software: you can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

VP is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with VP. If not, see
http://www.gnu.org/licenses/.
*/

/*
Включение всех файлов. Better Standard Library. © FXcoder

Библиотека не предназначена для отдельного распространения и использования.
Любая функция в следующей версии может быть исключена или изменена.

Include all files.

The library is not intended for separate distribution and use.
Any function in the next version can be excluded or changed.

todo:
	- calendar
	- signal
	- socket
*/

#property strict

#include "import/kernel32.mqh"
#include "import/user32.mqh"

#include "type/array/arraya.mqh"


#include "type/array/arrayptrt.mqh"


#include "type/chartcolors.mqh"
#include "type/dict.mqh"


#include "util/arr.mqh"


#include "util/color.mqh"
#include "util/conv.mqh"
#include "util/double.mqh"


#include "util/flag.mqh"
#include "util/hsv.mqh"
#include "util/math.mqh"
#include "util/ptr.mqh"
#include "util/ring.mqh"


#include "util/str.mqh"
#include "util/tf.mqh"
#include "util/time.mqh"
#include "util/util.mqh"


#include "chart.mqh"
#include "chartevent.mqh"


#include "debug.mqh"
#include "event.mqh"


#include "go.mqh"
#include "goselect.mqh"


#include "mql.mqh"

#include "series.mqh"

#include "symbol.mqh"

#include "symbolseries.mqh"


#include "window.mqh"
#include "chartwindow.mqh"

