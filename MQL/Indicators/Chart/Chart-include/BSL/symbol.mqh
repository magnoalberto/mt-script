/*
Copyright 2019 FXcoder

This file is part of Chart.

Chart is free software: you can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Chart is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with Chart. If not, see
http://www.gnu.org/licenses/.
*/

// Класс доступа к свойствам инструмента (символа), 5.2005/4.1601. Better Standard Library. © FXcoder

#property strict

#include "type/dict.mqh"
#include "util/math.mqh"
#include "util/ptr.mqh"
#include "util/sym.mqh"

class CBSymbol
{
private:

	// cache
	double volume_step_;
	double volume_min_;


protected:

	const string name_;

	// Словари с кэшем.
	// Не самый быстрый вариант. Самый - это отдельный член для каждой переменной с проверкой изменений (раз в 5-10 быстрее).
	// Незначительно (15-20%) быстрее вариант с массивами с прямой адресацией свойств (_cacheDoble[property_id]).
	CBDict<ENUM_SYMBOL_INFO_DOUBLE,  double> cache_double_;
	CBDict<ENUM_SYMBOL_INFO_INTEGER, long  > cache_long_;
	CBDict<ENUM_SYMBOL_INFO_STRING,  string> cache_string_;

	// Массивы свойств символа
	static const ENUM_SYMBOL_INFO_INTEGER integers_[];
	static const ENUM_SYMBOL_INFO_DOUBLE  doubles_[];
	static const ENUM_SYMBOL_INFO_STRING  strings_[];


public:

	void CBSymbol():
		name_(_Symbol)
	{
		init_cache();
	}

	void CBSymbol(string symbol):
		name_(_str.is_empty(symbol) ? _Symbol : symbol)
	{
		init_cache();
	}

	string name() const
	{
		return(name_);
	}

	// функции
	MqlTick tick()              const { MqlTick tick; ::SymbolInfoTick(name_, tick); return(tick); }
	bool    tick(MqlTick &tick) const { return(::SymbolInfoTick(name_, tick)); }
	bool    is_synchronized()   const { return(::SymbolIsSynchronized(name_)); }
	bool    select(bool select) const { return(::SymbolSelect(name_, select)); }

	// selected_only - искать только в выбранных (в обзоре рынка)
	bool exists(bool selected_only = false)
	{
		return(_sym.exists(name_, selected_only));
	}
	
	bool is_current()
	{
		return(_sym.is_current(name_));
	}

	bool is_trade_allowed(datetime time) const
	{
		//return(::IsTradeAllowed(name_, time));
		
		return(true);
	}

	/*
	Нормализовать лот.
	Лот будет ограничен сверху максимальным лотом, снизу - минимальным лотом и округлен до
	точности лота для данного инструмента.
	@param lot  Лот
	@return     Нормализованный лот.
	*/
	double normalize_volume(double volume)
	{
		volume = _math.round_err(volume, volume_step());

		double min_lot = volume_min();
		if (volume < min_lot)
			return(min_lot);
			
		double max_lot = volume_max();
		if (volume > max_lot)
			return(max_lot);

		return(volume);
	}

	bool is_forex()
	{
#ifdef __MQL4__
		return(get_cached(SYMBOL_TRADE_CALC_MODE) == 0);
#else
		ENUM_SYMBOL_CALC_MODE calc_mode = (ENUM_SYMBOL_CALC_MODE)get_cached(SYMBOL_TRADE_CALC_MODE);
		return((calc_mode == SYMBOL_CALC_MODE_FOREX) || (calc_mode == SYMBOL_CALC_MODE_FOREX_NO_LEVERAGE));
#endif
	}

	// для удобства, чтобы не запоминать где какой тип валюты, только для форекс
	string currency1() { return(get_cached(SYMBOL_CURRENCY_BASE)); }   // = currency_base
	string currency2() { return(get_cached(SYMBOL_CURRENCY_PROFIT)); } // = currency_profit

	// cached
	ENUM_SYMBOL_TRADE_EXECUTION   trade_exe_mode         () { return( (ENUM_SYMBOL_TRADE_EXECUTION) get_cached(SYMBOL_TRADE_EXEMODE) ); }
	ENUM_DAY_OF_WEEK              swap_rollover_3_days   () { return( (ENUM_DAY_OF_WEEK)            get_cached(SYMBOL_SWAP_ROLLOVER3DAYS) ); }

	string   currency_base                () { return(            get_cached(SYMBOL_CURRENCY_BASE)        ); } // = currency1
	string   currency_profit              () { return(            get_cached(SYMBOL_CURRENCY_PROFIT)      ); } // = currency2
	string   currency_margin              () { return(            get_cached(SYMBOL_CURRENCY_MARGIN)      ); }
	int      digits                       () { return( (int)      get_cached(SYMBOL_DIGITS)               ); }
	int      expiration_mode              () { return( (int)      get_cached(SYMBOL_EXPIRATION_MODE)      ); }
	datetime expiration_time              () { return( (datetime) get_cached(SYMBOL_EXPIRATION_TIME)      ); }
	string   path                         () { return(            get_cached(SYMBOL_PATH)                 ); } // r/o ?
	double   point                        () { return(            get_cached(SYMBOL_POINT)                ); }
	datetime start_time                   () { return( (datetime) get_cached(SYMBOL_START_TIME)           ); }
	double   volume_max                   () { return(            get_cached(SYMBOL_VOLUME_MAX)           ); }
	double   volume_min                   () { if (volume_min_  <= 0) { volume_min_  = get_cached(SYMBOL_VOLUME_MIN ); } return(volume_min_); }
	double   volume_step                  () { if (volume_step_ <= 0) { volume_step_ = get_cached(SYMBOL_VOLUME_STEP); } return(volume_step_); }

	// direct
	ENUM_SYMBOL_TRADE_MODE trade_mode     () const { return( (ENUM_SYMBOL_TRADE_MODE) get(SYMBOL_TRADE_MODE)   ); }
	double   ask                          () const { return(            get(SYMBOL_ASK)                        ); }
	double   ask_high                     () const { return(            get(SYMBOL_ASKHIGH)                    ); }
	double   ask_low                      () const { return(            get(SYMBOL_ASKLOW)                     ); }
	string   bank                         () const { return(            get(SYMBOL_BANK)                       ); }
	double   bid                          () const { return(            get(SYMBOL_BID)                        ); }
	double   bid_high                     () const { return(            get(SYMBOL_BIDHIGH)                    ); }
	double   bid_low                      () const { return(            get(SYMBOL_BIDLOW)                     ); }
	long     deals                        () const { return(            get(SYMBOL_SESSION_DEALS)              ); }
	string   description                  () const { return(            get(SYMBOL_DESCRIPTION)                ); }
	int      filling_mode                 () const { return( (int)      get(SYMBOL_FILLING_MODE)               ); }
	string   isin                         () const { return(            get(SYMBOL_ISIN)                       ); }
	double   last                         () const { return(            get(SYMBOL_LAST)                       ); }
	double   last_high                    () const { return(            get(SYMBOL_LASTHIGH)                   ); }
	double   last_low                     () const { return(            get(SYMBOL_LASTLOW)                    ); }
	double   margin_initial               () const { return(            get(SYMBOL_MARGIN_INITIAL)             ); }
	double   margin_maintenance           () const { return(            get(SYMBOL_MARGIN_MAINTENANCE)         ); }
	int      order_mode                   () const { return( (int)      get(SYMBOL_ORDER_MODE)                 ); }
	bool     select                       () const { return( (bool)     get(SYMBOL_SELECT)                     ); }
	double   session_aw                   () const { return(            get(SYMBOL_SESSION_AW)                 ); }
	long     session_buy_orders           () const { return(            get(SYMBOL_SESSION_BUY_ORDERS)         ); }
	double   session_buy_orders_volume    () const { return(            get(SYMBOL_SESSION_BUY_ORDERS_VOLUME)  ); }
	double   session_close                () const { return(            get(SYMBOL_SESSION_CLOSE)              ); }
	double   session_interest             () const { return(            get(SYMBOL_SESSION_INTEREST)           ); }
	double   session_open                 () const { return(            get(SYMBOL_SESSION_OPEN)               ); }
	double   session_price_limit_max      () const { return(            get(SYMBOL_SESSION_PRICE_LIMIT_MAX)    ); }
	double   session_price_limit_min      () const { return(            get(SYMBOL_SESSION_PRICE_LIMIT_MIN)    ); }
	double   session_price_settlement     () const { return(            get(SYMBOL_SESSION_PRICE_SETTLEMENT)   ); }
	long     session_sell_orders          () const { return(            get(SYMBOL_SESSION_SELL_ORDERS)        ); }
	double   session_sell_orders_volume   () const { return(            get(SYMBOL_SESSION_SELL_ORDERS_VOLUME) ); }
	double   session_turnover             () const { return(            get(SYMBOL_SESSION_TURNOVER)           ); }
	double   session_volume               () const { return(            get(SYMBOL_SESSION_VOLUME)             ); }
	int      spread                       () const { return( (int)      get(SYMBOL_SPREAD)                     ); }
	bool     spread_float                 () const { return( (bool)     get(SYMBOL_SPREAD_FLOAT)               ); }
	double   swap_long                    () const { return(            get(SYMBOL_SWAP_LONG)                  ); }
	double   swap_short                   () const { return(            get(SYMBOL_SWAP_SHORT)                 ); }
	int      ticks_book_depth             () const { return( (int)      get(SYMBOL_TICKS_BOOKDEPTH)            ); }
	datetime time                         () const { return( (datetime) get(SYMBOL_TIME)                       ); }
	double   trade_contract_size          () const { return(            get(SYMBOL_TRADE_CONTRACT_SIZE)        ); }
	int      trade_freeze_level           () const { return( (int)      get(SYMBOL_TRADE_FREEZE_LEVEL)         ); }
	int      trade_stops_level            () const { return( (int)      get(SYMBOL_TRADE_STOPS_LEVEL)          ); }
	double   trade_tick_value             () const { return(            get(SYMBOL_TRADE_TICK_VALUE)           ); }
	double   trade_tick_value_profit      () const { return(            get(SYMBOL_TRADE_TICK_VALUE_PROFIT)    ); }
	double   trade_tick_value_loss        () const { return(            get(SYMBOL_TRADE_TICK_VALUE_LOSS)      ); }
	double   trade_tick_size              () const { return(            get(SYMBOL_TRADE_TICK_SIZE)            ); }
	long     volume                       () const { return(            get(SYMBOL_VOLUME)                     ); }
	long     volume_high                  () const { return(            get(SYMBOL_VOLUMEHIGH)                 ); }
	double   volume_limit                 () const { return(            get(SYMBOL_VOLUME_LIMIT)               ); }
	long     volume_low                   () const { return(            get(SYMBOL_VOLUMELOW)                  ); }

	// Универсальные функции доступа к свойствам

	double get(ENUM_SYMBOL_INFO_DOUBLE  property_id) const { return(::SymbolInfoDouble (name_, property_id)); }
	long   get(ENUM_SYMBOL_INFO_INTEGER property_id) const { return(::SymbolInfoInteger(name_, property_id)); }
	string get(ENUM_SYMBOL_INFO_STRING  property_id) const { return(::SymbolInfoString (name_, property_id)); }

	bool get(ENUM_SYMBOL_INFO_DOUBLE  property_id, double &value) const { return(::SymbolInfoDouble (name_, property_id, value) ); }
	bool get(ENUM_SYMBOL_INFO_INTEGER property_id, long   &value) const { return(::SymbolInfoInteger(name_, property_id, value) ); }
	bool get(ENUM_SYMBOL_INFO_STRING  property_id, string &value) const { return(::SymbolInfoString (name_, property_id, value) ); }


protected:

	// double
	double get_cached(ENUM_SYMBOL_INFO_DOUBLE property_id)
	{
		double value;
		
		// Найти в кэше
		if (cache_double_.try_get_value(property_id, value))
			return(value);
				
		// Получить значение. Для совместимости в случае неудачи вернуть результат с прямым результатом
		if (!::SymbolInfoDouble(name_, property_id, value))
			return(::SymbolInfoDouble(name_, property_id));
		
		// В случае успеха записать значение в кэш
		cache_double_.set(property_id, value);
		return(value);
	}

	// integer
	long get_cached(ENUM_SYMBOL_INFO_INTEGER property_id)
	{
		long value;
		
		// Найти в кэше
		if (cache_long_.try_get_value(property_id, value))
			return(value);
		
		// Получить значение. Для совместимости в случае неудачи вернуть результат с прямым результатом
		if (!::SymbolInfoInteger(name_, property_id, value))
			return(::SymbolInfoInteger(name_, property_id));
		
		// В случае успеха записать значение в кэш
		cache_long_.set(property_id, value);
		return(value);
	}

	// string
	string get_cached(ENUM_SYMBOL_INFO_STRING property_id)
	{
		string value;
		
		// Найти в кэше
		if (cache_string_.try_get_value(property_id, value))
			return(value);
		
		// Получить значение. Для совместимости в случае неудачи вернуть результат с прямым результатом
		if (!::SymbolInfoString(name_, property_id, value))
			return(::SymbolInfoString(name_, property_id));
		
		// В случае успеха записать значение в кэш
		cache_string_.set(property_id, value);
		return(value);
	}

	void init_cache()
	{
		volume_step_ = -1;
		volume_min_ = -1;
	}

};


const ENUM_SYMBOL_INFO_INTEGER CBSymbol::integers_[] =
{
/*
	Универсальные     Только мт4          Только мт5          Разное значение в 4 и 5
*/
#ifndef __MQL4__
	                                      SYMBOL_CUSTOM,              // 5.1640?
	                                      SYMBOL_BACKGROUND_COLOR,    // 5.1730?
	                                      SYMBOL_CHART_MODE,          // 5.1730?
	                                      SYMBOL_EXIST,               // 5.2005
#endif
	SYMBOL_SELECT,
	SYMBOL_VISIBLE,
	SYMBOL_SESSION_DEALS,
	SYMBOL_SESSION_BUY_ORDERS,
	SYMBOL_SESSION_SELL_ORDERS,
	SYMBOL_VOLUME,
	SYMBOL_VOLUMEHIGH,
	SYMBOL_VOLUMELOW,
	SYMBOL_TIME,
	SYMBOL_DIGITS,
	SYMBOL_SPREAD,
	SYMBOL_SPREAD_FLOAT,
	SYMBOL_TICKS_BOOKDEPTH,
	                                                          SYMBOL_TRADE_CALC_MODE,
	SYMBOL_TRADE_MODE,
	SYMBOL_START_TIME,
	SYMBOL_EXPIRATION_TIME,
	SYMBOL_TRADE_STOPS_LEVEL,
	SYMBOL_TRADE_FREEZE_LEVEL,
	SYMBOL_TRADE_EXEMODE,
	SYMBOL_SWAP_MODE,
	SYMBOL_SWAP_ROLLOVER3DAYS,
#ifndef __MQL4__
	                                      SYMBOL_MARGIN_HEDGED_USE_LEG,    // 5.1730?
#endif
	SYMBOL_EXPIRATION_MODE,
	SYMBOL_FILLING_MODE,
	SYMBOL_ORDER_MODE,
#ifndef __MQL4__
	                                      SYMBOL_ORDER_GTC_MODE,           // 5.1730?
	                                      SYMBOL_OPTION_MODE,
	                                      SYMBOL_OPTION_RIGHT,
#endif
};

const ENUM_SYMBOL_INFO_DOUBLE CBSymbol::doubles_[] =
{
/*
	Универсальные     Только мт4          Только мт5
*/

	SYMBOL_BID,
	SYMBOL_BIDHIGH,
	SYMBOL_BIDLOW,
	SYMBOL_ASK,
	SYMBOL_ASKHIGH,
	SYMBOL_ASKLOW,
	SYMBOL_LAST,
	SYMBOL_LASTHIGH,
	SYMBOL_LASTLOW,
#ifndef __MQL4__
	                                      SYMBOL_OPTION_STRIKE,
#endif
	SYMBOL_POINT,
	SYMBOL_TRADE_TICK_VALUE,
	SYMBOL_TRADE_TICK_VALUE_PROFIT,
	SYMBOL_TRADE_TICK_VALUE_LOSS,
	SYMBOL_TRADE_TICK_SIZE,
	SYMBOL_TRADE_CONTRACT_SIZE,
#ifndef __MQL4__
	                                      SYMBOL_TRADE_ACCRUED_INTEREST,   // 5.1730?
	                                      SYMBOL_TRADE_FACE_VALUE,         // 5.1730?
	                                      SYMBOL_TRADE_LIQUIDITY_RATE,     // 5.1730?
#endif
	SYMBOL_VOLUME_MIN,
	SYMBOL_VOLUME_MAX,
	SYMBOL_VOLUME_STEP,
	SYMBOL_VOLUME_LIMIT,
	SYMBOL_SWAP_LONG,
	SYMBOL_SWAP_SHORT,
	SYMBOL_MARGIN_INITIAL,
	SYMBOL_MARGIN_MAINTENANCE,
#ifdef __MQL4__
	                  SYMBOL_MARGIN_LONG,
	                  SYMBOL_MARGIN_SHORT,
	                  SYMBOL_MARGIN_LIMIT,
	                  SYMBOL_MARGIN_STOP,
	                  SYMBOL_MARGIN_STOPLIMIT,
#endif
	SYMBOL_SESSION_VOLUME,
	SYMBOL_SESSION_TURNOVER,
	SYMBOL_SESSION_INTEREST,
	SYMBOL_SESSION_BUY_ORDERS_VOLUME,
	SYMBOL_SESSION_SELL_ORDERS_VOLUME,
	SYMBOL_SESSION_OPEN,
	SYMBOL_SESSION_CLOSE,
	SYMBOL_SESSION_AW,
	SYMBOL_SESSION_PRICE_SETTLEMENT,
	SYMBOL_SESSION_PRICE_LIMIT_MIN,
	SYMBOL_SESSION_PRICE_LIMIT_MAX,
#ifndef __MQL4__
	                                      SYMBOL_MARGIN_HEDGED,
#endif
};

const ENUM_SYMBOL_INFO_STRING CBSymbol::strings_[] =
{
/*
	Универсальные     Только мт4          Только мт5
*/
#ifndef __MQL4__
	                                      SYMBOL_BASIS,
#endif
	SYMBOL_CURRENCY_BASE,
	SYMBOL_CURRENCY_PROFIT,
	SYMBOL_CURRENCY_MARGIN,
	SYMBOL_BANK,
	SYMBOL_DESCRIPTION,
#ifndef __MQL4__
	                                      SYMBOL_FORMULA,             // 5.1730?
#endif
	SYMBOL_ISIN,
#ifndef __MQL4__
	                                      SYMBOL_PAGE,                // 5.1730?
#endif
	SYMBOL_PATH,
};


CBSymbol _symbol;


#ifdef __MQL4__

class CBSymbol4: public CBSymbol
{
public:

	void CBSymbol4():              CBSymbol()       { }
	void CBSymbol4(string symbol): CBSymbol(symbol) { }

	int trade_calc_mode () const { return((int)get(SYMBOL_TRADE_CALC_MODE)); } // 0 - Forex; 1 - CFD; 2 - Futures; 3 - CFD на индексы (см. MODE_MARGINCALCMODE)

} _symbol4;

#else

class CBSymbol5: public CBSymbol
{
public:

	void CBSymbol5():              CBSymbol()       { }
	void CBSymbol5(string symbol): CBSymbol(symbol) { }

	bool margin_rate(
		ENUM_ORDER_TYPE  order_type,               // тип ордера
		double          &initial_margin_rate,      // коэффициент взимания начальной маржи
		double          &maintenance_margin_rate   // коэффициент взимания поддерживающей маржи
		)
	{
		return(SymbolInfoMarginRate(name_, order_type, initial_margin_rate, maintenance_margin_rate));
	}

	// Свойства
	ENUM_SYMBOL_CALC_MODE    trade_calc_mode () const { return( (ENUM_SYMBOL_CALC_MODE)    get(SYMBOL_TRADE_CALC_MODE) ); } // в 4 другой результат
	ENUM_SYMBOL_OPTION_MODE  option_mode     () const { return( (ENUM_SYMBOL_OPTION_MODE)  get(SYMBOL_OPTION_MODE)     ); }
	ENUM_SYMBOL_OPTION_RIGHT option_right    () const { return( (ENUM_SYMBOL_OPTION_RIGHT) get(SYMBOL_OPTION_RIGHT)    ); }
	ENUM_SYMBOL_SWAP_MODE    swap_mode       () const { return( (ENUM_SYMBOL_SWAP_MODE)    get(SYMBOL_SWAP_MODE)       ); }

	string   basis                       () const { return( get(SYMBOL_BASIS)         ); }
	double   option_strike               () const { return( get(SYMBOL_OPTION_STRIKE) ); }
	double   session_margin_hedged       () const { return( get(SYMBOL_MARGIN_HEDGED) ); }

	// 5.1640+
	bool     custom() { return((bool)get_cached(SYMBOL_CUSTOM)); } // r/o?

	// 5.1730+, 4?
	string   formula () const { return(        get(SYMBOL_FORMULA)); }
	string   page    () const { return(        get(SYMBOL_PAGE));    }
	bool     visible () const { return( (bool) get(SYMBOL_VISIBLE)); } // r/o?

	// 5.1910
	double   volume_real     () const { return( get(SYMBOL_VOLUME_REAL)     ); }
	double   volumehigh_real () const { return( get(SYMBOL_VOLUMEHIGH_REAL) ); }
	double   volumelow_real  () const { return( get(SYMBOL_VOLUMELOW_REAL)  ); }

	// 5.2005
	bool     exist           () const { return((bool)get(SYMBOL_EXIST)); }

} _symbol5;

#endif

