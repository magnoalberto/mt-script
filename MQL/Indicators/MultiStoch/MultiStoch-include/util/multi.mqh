/*
Copyright 2019 FXcoder

This file is part of MultiStoch.

MultiStoch is free software: you can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MultiStoch is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with MultiStoch. If not, see
http://www.gnu.org/licenses/.
*/

// Функции для унификации мультипериодных индикаторов. © FXcoder

#property strict

#include "../bsl.mqh"
#include "../enum/step.mqh"


// Инициализировать набор периодов, заданный вручную. Вернуть количество периодов и их набор, не более max_line_count.
int multi_get_periods_user(string user_periods, int max_line_count, int &periods[])
{
	string parts[];
	user_periods = _str.replace(user_periods, " ", ",");
	user_periods = _str.replace(user_periods, ";", ",");
	int period_count = _str.split(user_periods, ",", true, false, parts);

	int real_count = 0;
	ArrayFree(periods);

	for (int i = 0; i < period_count; i++)
	{
		int period = (int)StringToInteger(parts[i]);

		real_count = _arr.add(periods, period, 100) + 1;
		
		// жесткое ограничение на количество периодов
		if (real_count == max_line_count)
			break;
	}

	return(real_count);
}


// Расчет периодов для индикаторов серии Multi*
int multi_get_periods_range_linear(int period_start, int period_end, int line_count, int &periods[])
{
	ArrayResize(periods, line_count);

	double step = 1.0 * (period_end - period_start) / (line_count - 1);

	for (int j = 0; j < line_count; j++)
		periods[j] = (int)MathRound(1.0 * period_start + j * step);

	return(line_count);
}


// Расчет периодов для индикаторов серии Multi*
int multi_get_periods_range_exponential(int period_start, int period_end, int line_count, int &periods[])
{
	ArrayResize(periods, line_count);

	// расчет экспоненциально изменяющегося набора периодов по крайним значениям
	double a = 0;
	double b = 0;

	b = MathPow(1.0 * period_end / period_start, 1.0 / (line_count - 1));
	a = period_start;

	for (int i = 0; i < line_count; i++)
		periods[i] = (int)MathRound(a * MathPow(b, i));

	return(line_count);
}

// Расчет параболически изменяющегося набора периодов по крайним значениям для индикаторов серии Multi*
int multi_get_periods_range_parabolic(int period_start, int period_end, int line_count, int &periods[])
{
	ArrayResize(periods, line_count);

	if (period_start == period_end)
	{
		ArrayInitialize(periods, period_start);
		return(line_count);
	}

	// для упрощения вида формулы
	int p1 = period_start;
	int p2 = period_end;
	int x1 = 0;
	int x2 = line_count - 1;

	double b = (p1 * x2 - p2 * x1 + (x2 - x1) * sqrt(p1 * p2)) / (p2 - p1);
	double a = p1 / (b + x1) / (b + x1);

	for (int i = 0; i < line_count; i++)
		periods[i] = _math.round_to_int(a * (b + i) * (b + i));

	return(line_count);
}


int multi_get_periods(int period_start, int period_end, int line_count, ENUM_STEP step_type, string user_periods, int &periods[])
{
	if (_str.trim(user_periods) != "")
		return(multi_get_periods_user(user_periods, INT_MAX, periods));

	ArrayFree(periods);

	if ((line_count <= 0) || (period_start < 0) || (period_end < 0))
		return(0);

	if (line_count == 1)
		return(_arr.add(periods, period_start, 100) + 1);

	//if (period_start == period_end)
	//	return(ArrayAdd(periods, period_start) + 1);
		
//
//	{
//		ArrayResize(periods, line_count);
//		ArrayInitialize(periods, period_start);
//		return(line_count);
//	}

	switch (step_type)
	{
		case STEP_LINEAR:     return(multi_get_periods_range_linear(period_start, period_end, line_count, periods));
		case STEP_EXP:        return(multi_get_periods_range_exponential(period_start, period_end, line_count, periods));
		case STEP_PARABOLIC:  return(multi_get_periods_range_parabolic(period_start, period_end, line_count, periods));
		
		case STEP_PAR_EXP:
			{
				int exp_periods[];
				int exp_count = multi_get_periods_range_exponential(period_start, period_end, line_count, exp_periods);
				
				int par_periods[];
				int par_count = multi_get_periods_range_parabolic(period_start, period_end, line_count, par_periods);
				
				if (exp_count != par_count)
					return(0);
				
				ArrayResize(periods, exp_count);
				
				for (int i = 0; i < exp_count; i++)
					periods[i] = _math.round_to_int((double)(exp_periods[i] + par_periods[i]) / 2.0);
				
				return(exp_count);
			}
		
		case STEP_LIN_PAR:
			{
				int lin_periods[];
				int lin_count = multi_get_periods_range_linear(period_start, period_end, line_count, lin_periods);
				
				int par_periods[];
				int par_count = multi_get_periods_range_parabolic(period_start, period_end, line_count, par_periods);
				
				if (lin_count != par_count)
					return(0);
				
				ArrayResize(periods, lin_count);
				
				for (int i = 0; i < lin_count; i++)
					periods[i] = _math.round_to_int((double)(lin_periods[i] + par_periods[i]) / 2.0);
				
				return(lin_count);
			}
	}

	return(0);
}


string multi_get_periods_string(const string customPeriods, const int &periods[], const ENUM_STEP step)
{
	return(
		(customPeriods != "")
			? _arr.to_string(periods, ",")
			: _conv.range_to_string(periods, enum_step_to_string(step, true))
		);
}

