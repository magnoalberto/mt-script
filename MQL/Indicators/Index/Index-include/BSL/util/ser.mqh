/*
Copyright 2019 FXcoder

This file is part of Index.

Index is free software: you can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Index is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with Index. If not, see
http://www.gnu.org/licenses/.
*/

// Функции серий, исторических данных. Better Standard Library. © FXcoder
// В отличие от класса CBSeries, здесь общие функции без привязки к символу или таймфрейму.

//      (если оно то же самое, то считать это повторным вызовом на том же новом баре)


#property strict

#include "../series.mqh"
#include "tf.mqh"


class CBSeriesUtil
{
private:

	static datetime prev_open_time_;


public:

	static bool is_new_bar(datetime time, datetime &prev_open_time)
	{
		time = bar_open_time(time);
		
		if (time == prev_open_time)
			return(false);
		
		prev_open_time = time;
		return(true);
	}

	// uses .prev_open_time_
	static bool is_new_bar(datetime time)
	{
		return(is_new_bar(time, prev_open_time_));
	}

	// uses .prev_open_time_
	static bool is_new_bar()
	{
		return(is_new_bar(::TimeCurrent()));
	}


	static datetime bar_open_time(datetime time, ENUM_TIMEFRAMES period)
	{
		int ps = ::PeriodSeconds(period);
		return((time / ps) * ps);
	}

	static datetime bar_open_time(datetime time)
	{
		return((time / _tf.seconds) * _tf.seconds);
	}

	static datetime bar_open_time()
	{
		return(bar_open_time(::TimeCurrent()));
	}

};

datetime CBSeriesUtil::prev_open_time_ = 0;

CBSeriesUtil _ser;

