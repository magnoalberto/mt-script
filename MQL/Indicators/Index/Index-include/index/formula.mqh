/*
Copyright 2019 FXcoder

This file is part of Index.

Index is free software: you can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Index is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with Index. If not, see
http://www.gnu.org/licenses/.
*/

// Хранение формулы индекса, как в общем смысле (EURUSD*USD, EURUSD*GBPUSD), так и отдельно только индексов (EUR, USD,..). © FXcoder

#property strict

#include "../bsl.mqh"
#include "../s.mqh"
#include "context.mqh"
#include "element.mqh"


class CIndexFormula
{
protected:

	CIndexFormulaElement *elements_[];
	const CIndexContext  *context_;


public:

	// Конструкторы
	void CIndexFormula(const CIndexContext* &context):
		context_(context)
	{
		clear();
	}

	void CIndexFormula(const CIndexFormula &formula, const CIndexContext* &context):
		context_(context)
	{
		clear();
		add(formula);
	}
	
	void CIndexFormula(const CIndexFormula &formula, double factor, const CIndexContext* &context):
		context_(context)
	{
		clear();
		add(formula, factor);
	}
	
	void CIndexFormula(const CIndexFormula &formulas[], const CIndexContext* &context):
		context_(context)
	{
		clear();
		add(formulas);
	}
	
	void CIndexFormula(const CIndexFormula &formulas[], double factor, const CIndexContext* &context):
		context_(context)
	{
		clear();
		add(formulas, factor);
	}

	void ~CIndexFormula()
	{
		_ptr.safe_delete_array(elements_);
	}

	int length() const
	{
		return(ArraySize(elements_));
	}
	
	CIndexFormulaElement *operator[](int i) const
	{
		return(elements_[i]);
	}
	

	// очистить формулу
	void clear()
	{
		_ptr.safe_delete_array(elements_);
		_arr.resize(elements_, 0);
	}

	// Добавить элемент формулы.
	// Если элемент с таким символом уже есть, то будет изменён множитель, иначе добавится новый элемент.
	// Основной вариант add(), остальные так или иначе приходят к вызову этого.
	int add(string symbol, double factor)
	{
		int pos = search_symbol(symbol);
		
		if (pos < 0)
		{
			CIndexFormulaElement *element = new CIndexFormulaElement(symbol, factor, context_);
			pos = _arr.add(elements_, element);
			
			if (pos < 0)
			{
				_ptr.safe_delete(element);
				_debug.warning("!_arr.add");
				return(-1);
			}
		}
		else
		{
			elements_[pos].plus(factor);
		}
		
		return(pos);
	}

	void add(const CIndexFormulaElement &element)
	{
		add(element.symbol(), element.factor());
	}

	// Добавить в эту формулу другую формулу.
	void add(const CIndexFormula &formula)
	{
		for (int i = 0, count = formula.length(); i < count; i++)
			add(formula[i].symbol(), formula[i].factor());
	}

	// Добавить в эту формулу другую формулу и умножить на коэффициент.
	void add(const CIndexFormula &formula, double factor)
	{
		for (int i = 0, count = formula.length(); i < count; i++)
			add(formula[i].symbol(), formula[i].factor() * factor);
	}
	
	void add(const CIndexFormula &formulas[])
	{
		for (int i = 0, count = ArraySize(formulas); i < count; i++)
			add(formulas[i]);
	}

	void add(const CIndexFormula &formulas[], double factor)
	{
		for (int i = 0, count = ArraySize(formulas); i < count; i++)
			add(formulas[i], factor);
	}

	// если указан count, то проверок на совместимость массивов по длине не производится
	void add(const CIndexFormula &formulas[], const double &factors[])
	{
		int count = ArraySize(formulas);
		
		// Если длина не указана явно и длины массивов формул и множителей не совпадают, то что-то не так, выходим
		if (count != ArraySize(factors))
			return;
		
		for (int i = 0; i < count; i++)
			add(formulas[i], factors[i]);
	}

	// Умножить все множители на заданное значение
	void multiply_by(double factor)
	{
		for (int i = length() - 1; i >= 0; i--)
			elements_[i].multiply_by(factor);
	}

	// Получить строковую формулу в виде `±k1*sym1±k2*sym2...`
	string to_string(int digits = 8) const
	{
		string res = "";

		for (int i = 0, count = length(); i < count; i++)
			res += _double.to_string_sign(elements_[i].factor(), digits, "+") + "*" + elements_[i].symbol();

		return(res);
	}


private:

	int search_symbol(string symbol)
	{
		for (int i = 0, count = length(); i < count; i++)
		{
			if (symbol == elements_[i].symbol())
				return(i);
		}
		
		return(-1);
	}

};
