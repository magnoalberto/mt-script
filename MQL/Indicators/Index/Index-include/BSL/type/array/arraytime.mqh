/*
Copyright 2019 FXcoder

This file is part of Index.

Index is free software: you can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Index is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with Index. If not, see
http://www.gnu.org/licenses/.
*/

// Класс массива datetime. Better Standard Library. © FXcoder

#property strict

#include "../../util/arr.mqh"
#include "arraynumat.mqh"


class CBArrayTime: public CBArrayNumAT<datetime>
{
public:

	datetime data[];


public:

	void CBArrayTime():                      CBArrayNumAT() { }
	void CBArrayTime(int size):              CBArrayNumAT() { size(size);          }
	void CBArrayTime(int size, int reserve): CBArrayNumAT() { size(size, reserve); }

	/* Implementation */
	
	virtual int size()                      override const { return(::ArraySize(data)); }
	virtual int size(int size)              override       { return(::ArrayResize(data, size, reserve_)); };
	virtual int size(int size, int reserve) override       { reserve_ = reserve; return(size(size)); }

	virtual datetime operator[](int i) const override { return(data[i]); } // не использовать, если важна скорость

	virtual int index_of(datetime value, int starting_from = 0) const override { return(_arr.index_of(data, value, starting_from)); }

	virtual void init(datetime value) override { _arr.init(data, value); }
	virtual int  add (datetime value) override { return(_arr.add(data, value, reserve_)); }

	virtual void clone(const datetime &src[]) override { _arr.clone(data, src); }
	virtual int  copy (const datetime &src[]) override { return(::ArrayCopy(data, src)); }
	virtual int  copy (const datetime &src[], int dst_start, int src_start, int count) override { return(::ArrayCopy(data, src, dst_start, src_start, count)); }

	virtual int      max      (int first, int count) const override { return(_arr.max(data, first, count)); }
	virtual int      min      (int first, int count) const override { return(_arr.min(data, first, count)); }
	virtual datetime max_value(int first, int count) const override { return(_arr.max_value(data, first, count)); }
	virtual datetime min_value(int first, int count) const override { return(_arr.min_value(data, first, count)); }

	virtual int      max      () const override { return(_arr.max(data)); }
	virtual int      min      () const override { return(_arr.min(data)); }
	virtual datetime max_value() const override { return(_arr.max_value(data)); }
	virtual datetime min_value() const override { return(_arr.min_value(data)); }

	virtual string to_string(string separator) const override { return(_arr.to_string(data, separator, TIME_DATE | TIME_MINUTES)); }

	/* Type-dependent functions */

	string to_string(string separator, int flags) const { return(_arr.to_string(data, separator, flags)); }

};
