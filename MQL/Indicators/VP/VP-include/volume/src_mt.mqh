/*
Copyright 2019 FXcoder

This file is part of VP.

VP is free software: you can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

VP is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with VP. If not, see
http://www.gnu.org/licenses/.
*/

// Источник данных - MetaTrader. © FXcoder

#property strict

#include "../bsl.mqh"
#include "../enum/applied_volume.mqh"
#include "enum/vp_source.mqh"

#ifndef __MQL4__

/**
Получить (вычислить) гистограмму по тикам
*/
int get_hg_by_ticks(datetime time_from, datetime time_to, double point, ENUM_APPLIED_VOLUME applied_volume, double &low, double &volumes[])
{
	ResetLastError();

	// Скопировать тики, нужны только совершённые сделки, Last + объём + время тика
	MqlTick ticks[];
	int tick_count = CopyTicksRange(_Symbol, ticks, COPY_TICKS_ALL, time_from * 1000, time_to * 1000 - 1);

	// Нет тиков - нет гистограммы
	if (tick_count <= 0)
		PRINT_RETURN("!CopyTicksRange" + VAR(tick_count) + VAR(time_from) + VAR(time_to), 0);

	// Определить минимум и максимум, размер массива гистограммы
	MqlTick tick = ticks[0];
	low = _math.round_err(tick.bid, point);
	double high = low;
	long time_to_ms = time_to * 1000;

	for (int i = 1; i < tick_count; i++)
	{
		tick = ticks[i];
		
		// Иногда полученные тики выходят за указанный диапазон (ошибка в данных на сервере, например).
		// В таком случае обрезать лишнее. Сам массив можно не изменять, достаточно уточнить количество правильных тиков.
		// Предположительно проблемы выхода за левую границу нет.
		if (tick.time_msc > time_to_ms)
		{
			tick_count = i;
			break;
		}
		
		double tick_last = _math.round_err(tick.bid, point);
		
		if (tick_last < low)
			low = tick_last;
			
		if (tick_last > high)
			high = tick_last;
	}

	int low_points = price_to_points(low, point);
	int high_points = price_to_points(high, point);
	int hg_size = high_points - low_points + 1; // количество цен в гистограмме
	ArrayResize(volumes, hg_size);
	ArrayInitialize(volumes, 0);


	// Сложить все тики в одну гистограмму

	int pri;

	for (int j = 0; j < tick_count; j++)
	{
		tick = ticks[j];
		pri = price_to_points(tick.bid, point) - low_points;

		// Если нужен реальный объём, берём его из информации по тику.
		// Если нужен тиковый объём, то достаточно учесть каждый тик ровно один раз.
		volumes[pri] += (applied_volume == VOLUME_REAL) ? (double)tick.volume : 1;
	}

	LOG(VAR(tick_count) + " (" + DoubleToString(tick_count / 1000000.0, 3) + "M), vol:" + (string)_math.sum(volumes) + ". " + _err.last_message());

	return(hg_size);
}
#endif

/**
Получить (вычислить) гистограмму по барам, имитируя тики
*/
int get_hg(datetime time_from, datetime time_to, double point, ENUM_TIMEFRAMES data_period, ENUM_APPLIED_VOLUME applied_volume, double &low, double &volumes[])
{
	ResetLastError();

	// Получить бары таймфрейма расчёта (обычно M1)
	MqlRates rates[];
	int rate_count = CopyRates(_Symbol, data_period, time_from, time_to, rates);

	if (rate_count <= 0)
	{
		LOG(VAR(rate_count) + ". " + _err.last_message());
		return(0);
	}

	// Определить минимум и максимум, размер массива гистограммы
	MqlRates rate = rates[0];
	low = _math.round_err(rate.low, point);
	double high = _math.round_err(rate.high, point);

	for (int i = 1; i < rate_count; i++)
	{
		rate = rates[i];
		
		double rate_high =  _math.round_err(rate.high, point);
		double rate_low = _math.round_err(rate.low, point);
		
		if (rate_low < low)
			low = rate_low;
			
		if (rate_high > high)
			high = rate_high;
	}

	int low_index = price_to_points(low, point);
	int high_index = price_to_points(high, point);
	int hg_size = high_index - low_index + 1; // количество цен в гистограмме
	ArrayResize(volumes, hg_size);
	ArrayInitialize(volumes, 0);


	// Сложить все тики всех баров в одну гистограмму

	int pri, oi, hi, li, ci;
	double dv, v;

	for (int j = 0; j < rate_count; j++)
	{
		rate = rates[j];
		
		oi = price_to_points(rate.open, point) - low_index;
		hi = price_to_points(rate.high, point) - low_index;
		li = price_to_points(rate.low, point) - low_index;
		ci = price_to_points(rate.close, point) - low_index;
		
		v = (applied_volume == VOLUME_REAL) ? (double)rate.real_volume : (double)rate.tick_volume;
		
		// имитация тиков внутри бара
		if (ci >= oi)
		{
			/* бычья свеча */
			
			// средний объём каждого тика
			dv = v / (oi - li + hi - li + hi - ci + 1.0);

			// open --> low
			for (pri = oi; pri >= li; pri--)
				volumes[pri] += dv;

			// low+1 ++> high
			for (pri = li + 1; pri <= hi; pri++)
				volumes[pri] += dv;
			
			// high-1 --> close
			for (pri = hi - 1; pri >= ci; pri--)
				volumes[pri] += dv;
		}
		else
		{
			/* медвежья свеча */
			
			// средний объём каждого тика
			dv = v / (hi - oi + hi - li + ci - li + 1.0);

			// open ++> high
			for (pri = oi; pri <= hi; pri++)
				volumes[pri] += dv;
			
			// high-1 --> low
			for (pri = hi - 1; pri >= li; pri--)
				volumes[pri] += dv;
			
			// low+1 ++> close
			for (pri = li + 1; pri <= ci; pri++)
				volumes[pri] += dv;
		}
	}

	return(hg_size);
}

/**
Получить время первых доступных данных
*/
datetime get_horizon(ENUM_VP_SOURCE data_source, ENUM_TIMEFRAMES data_period)
{
#ifndef __MQL4__
	if (data_source == VP_SOURCE_TICKS)
	{
		MqlTick ticks[];
		int tick_count = CopyTicks(_Symbol, ticks, COPY_TICKS_INFO, 1, 1);

		// Если данных нет, вернуть время, равное текущему + 1 с
		if (tick_count <= 0)
			return ((datetime)(SymbolInfoInteger(_Symbol, SYMBOL_TIME) + 1));
			
		return(ticks[0].time);
	}
#endif

	return((datetime)(iTime(_Symbol, data_period, Bars(_Symbol, data_period) - 1)));
}

int price_to_points(double price, double point)
{
	return((int)(price / point + 0.5));
}
