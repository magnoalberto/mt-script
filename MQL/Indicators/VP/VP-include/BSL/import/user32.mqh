/*
Copyright 2019 FXcoder

This file is part of VP.

VP is free software: you can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

VP is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with VP. If not, see
http://www.gnu.org/licenses/.
*/

// Импортируемые функции user32.dll. Better Standard Library. © FXcoder

#property strict

#import "user32.dll"

// Messages
int PostMessageA(int hWnd, int Msg, int wParam, int lParam);

// Windows
int SetWindowPos(int hWnd, int hWndInsertAfter, int X, int Y, int cx, int cy, int uFlags);
int ShowWindow(int hWnd, int nCmdShow);

// etc
int GetAncestor(int hWnd, int gaFlags);
int GetWindowLongA(int hWnd, int nIndex);
int SetWindowLongA(int hWnd, int nIndex, int dwNewLong);

#import

