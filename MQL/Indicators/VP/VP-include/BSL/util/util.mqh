/*
Copyright 2019 FXcoder

This file is part of VP.

VP is free software: you can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

VP is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with VP. If not, see
http://www.gnu.org/licenses/.
*/

// Utils. Better Standard Library. � FXcoder

#property strict

#include "ptr.mqh"

class CBUtil
{
public:

	template <typename T>
	static void swap(T &a, T &b)
	{
		T tmp = a;
		a = b;
		b = tmp;
	}

	template <typename T>
	static int compare_ref(const T &a, const T &b)
	{
		return(a < b ? -1 : (a > b ? 1 : 0));
	}


} _bsl;
