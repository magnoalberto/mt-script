/*
Copyright 2019 FXcoder

This file is part of Chart.

Chart is free software: you can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Chart is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with Chart. If not, see
http://www.gnu.org/licenses/.
*/

// Функции символа (инструмента). Better Standard Library. © FXcoder

#property strict

#include "str.mqh"


class CBSymbolUtil
{
public:

	static const string current;


public:

	static bool is_current(string symbol)
	{
		return(_str.is_empty(symbol) || (symbol == _sym.current));
	}

	// selected_only - искать только в выбранных (в обзоре рынка)
	//tested: 4.1170
	static bool exists(string symbol, bool selected_only = false)
	{
		// Если свойство успешно считано, символ существет
		long tmp;
		if (::SymbolInfoInteger(symbol, SYMBOL_DIGITS, tmp))
			return(true);
		
		// проверка на ERR_MARKET_NOT_SELECTED здесь не нужна, т.к. код выше её игнорирует (5.2005)
		
		return(false);
	}

};

const string CBSymbolUtil::current  = Symbol();


CBSymbolUtil _sym;
